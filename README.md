* Descrição da aplicação

Nesta aplicação eu utilizei a linguagem Java com Spring boot para construir as API solicitadas.

Para armazenar os dados de Bots e Messages eu utilizei o Elasticsearch pois é um banco escalavel e rápido para realizar pesquisas com grande volume de dados, pois será armazenado um grande volume de mensagens.

Para realizar os testes end-to-end eu utilizei o client Postman para consumir todas as APIs criadas.

Para realizar o build automático desse projeto em um ambiente de produção eu criaria um Pipeline no Jenkins com os seguintes stages:

* Quando o usuário realizasse algum merge para a Branch 'master' realizaria o checkot do fonte.
* Realizaria o build do aplicação utilizando o maven, e então com o artefao criado pelo build enviaria ele para o servidor de produção.

Para escalar essa aplicação, seria necessário escalar o banco Elasticsearch criando um cluster para distribuir o acesso entre os nós, 
e também o artefato gerado pelo build poderia ser distribuído entre várias máquinas, pois todos eles se conectariam ao cluster criado pela Elasticsearch.

Para realizar a tolerância a falhas, eu utilizaria um gerenciador de serviços que ficaria monitorando as aplicações, caso alguma viesse a falhar, ela seria reiniciada de maneira automática.

Antes de enviar a produção poderia ser utilizado um ambiente de QA, para realizar os testes necessários e homologar a aplicação, onde o mesmo não afetaria os dados de produção.

  
* Como rodar a aplicação localmente

Para executar a aplicação será necessário instalar o Java versão 8, maven, e o Elasticsearch versão utilizada 7.5.5
Após isso entre na pasta do Elasticsearch e no diretório raiz, procure pela pasta config e abra o arquivo elasticsearch.yml dentro do arquivo localize a linha que altera o nome do cluster e altere para 'oportunidade-vivo'
-> cluster.name: oportunidade-vivo

Edite também a linha que altera o nome do nó do Elasticsearch local, e altere o nome do nó para 'node-vivo1'
-> node.name: node-vivo1

Para iniciar o Elasticsearch localize a pasta bin no diretorio raiz e rode o arquivo elasticsearch ou elasticsearch.bat, de acordo com o sistema operacional

Dentro da pasta do projeto, no diretório raiz utilize o comando 'mvn clean install' para fazer o build da aplicação e criar o artefato 'oportunidade-vivo-1.0.0.jar' na pasta 'target'.
Dentro da pasta 'target' utilize o comando 'java -jar oportunidade-vivo-1.0.0.jar', para executar a aplicação. Antes de realizar esse passo certifique que o Elasticsearch está em execução. 

* Devido a restrição de tempo eu não consegui implementar essa aplicação seguindo a arquitetura de micro serviços, onde eu separaria um micro serviço para realizar as operações no recurso de Bots e outro para operações no recurso de Messages,
melhorando assim a escalabilidade da aplicação.

* Como usar os recursos da API

* Bots

-> Criar novo Bot

[POST] /bots
{ "name": "Bot 1" }

-> Buscar todos os Bots

[GET] /bots

-> Buscar Bot por ID

[GET] /bots/:id

-> Atualizar Bot

[PUT] /bots/:id

{ "name": "Bot atualizado" }

-> Excluir Bot

[DELETE] /bots/:id

* Messages

-> Criar nova mensagem

[POST] /messages

{
    "conversationId": "88bc4fd4-ff27-430c-8e81-2d83d2eac7be",
    "timestamp": "2019-12-09T09:00:00.5926721Z",
    "from": "36b9f842-ee97-11e8-9443-0242ac120002",
    "to": "16edd3b3-3f75-40df-af07-2a3813a79ce9",
    "text": "Texto da mensagem!"
}

-> Buscar mensagem por ID

[GET] /messages/a9133ace-2995-45ed-bb90-cc28ace57525

-> Buscar mensagens por conversationId

[GET] /messages?conversationId=88bc4fd4-ff27-430c-8e81-2d83d2eac7be